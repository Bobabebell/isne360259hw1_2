# Todo API

This API provides support to Todo service


## Todo management 

* GET /api/todo/?sessionid={sessionid}        get all todo with session id
* GET /api/todo/{id}?sessionid={sessionid}      get todo with id, require session id
* POST /api/todo/?sessionid={sessionid}       create new todo, require session id

    payload: 
    ```Javascript
    {
        "Name": "{todo name/content}",
        "IsComplete": "{completed?}"
    }
    ```
*  PUT /api/todo/{id}?sessionid={sessionid}    update todo with id, require session id

    payload: 
    ```Javascript
    {
        "Name": "{todo name/content}",
        "IsComplete": "{completed?}"
    }
    ```

* DELETE /api/todo/{id}?sessionid={sessionid}     delete todo with id, require session id
